import React from "react";
import { Banner, Upload, Grid, Footer, Modal} from './components'

function App() {
  const [selectedImg, setSelectedImg] = React.useState(null);
  return (
    <div className="app">
      <Banner />
      <Upload />
      <Grid setSelectedImg={setSelectedImg} />
      {selectedImg && (
        <Modal selectedImg={selectedImg} setSelectedImg={setSelectedImg} />
      )}
      <Footer />
    </div>
  );
}

export default App;
