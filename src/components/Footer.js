import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <section>
        <h4>Demo by Artyom Ulanchik &copy; 2020</h4>
      </section>
    </footer>
  );
};

export default Footer;
