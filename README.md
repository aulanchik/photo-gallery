### Photo Gallery

[![Netlify Status](https://api.netlify.com/api/v1/badges/3431d4cf-ed36-49e7-b66c-6262c1e186e8/deploy-status)](https://app.netlify.com/sites/photo-memories/deploys)

This is a photo gallery tool which allows you to upload and see other images uploaded by the other users.

## Tech stack

- React (incl. React Hooks)
- Firebase
- HTML5
- CSS

### Demo access

This example was deployed live and accessible [here](https://photo-memories.netlify.app/)
